<?php
/**
 * Represents charities and related methods
 */

/**
 * Class SX_Charities
 */
class SX_Charities extends SX_Main {

    public $post_type;
    public $show_in_menu;

    /**
     * SX_Charities constructor.
     * @param $args
     * @param bool $show_in_menu
     */
    public function __construct( $args, $show_in_menu = true ) {
        parent::__construct( $args );

        if ( !empty( $this->post_types["charity"] ) ) $this->post_type = $this->post_types["charity"];
        $this->show_in_menu = $show_in_menu;

        // Register SX Charities Post Type
        add_action( 'init', array( $this, 'sx_charities_post_type' ) );
    }

    /**
     * Set parameters for and register sx_charity post type
     */
    public function sx_charities_post_type() {

        // Set UI labels for SX Charities Post Type
        $labels = array(
            'name'                => _x( 'Charities', 'Post Type General Name', $this->textdomain ),
            'singular_name'       => _x( 'Charity', 'Post Type Singular Name', $this->textdomain ),
            'menu_name'           => __( 'Charities', $this->textdomain ),
            'parent_item_colon'   => __( 'Parent Charity', $this->textdomain ),
            'all_items'           => __( 'Charities', $this->textdomain ),
            'view_item'           => __( 'View Charity', $this->textdomain ),
            'add_new_item'        => __( 'Add New Charity', $this->textdomain ),
            'add_new'             => __( 'Add New', $this->textdomain ),
            'edit_item'           => __( 'Edit Charity', $this->textdomain ),
            'update_item'         => __( 'Update Charity', $this->textdomain ),
            'search_items'        => __( 'Search Charities', $this->textdomain ),
            'not_found'           => __( 'Not Found', $this->textdomain ),
            'not_found_in_trash'  => __( 'Not found in Trash', $this->textdomain ),
        );

        $supports = array(
            'title',
        );

        // Set other options for SX Charities Post Type
        $args = array(
            'description'         => __( 'SX Charities', $this->textdomain ),
            'labels'              => $labels,
            'supports'            => $supports,
            'taxonomies'          => array(),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => $this->show_in_menu,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 11,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
        );

        // Registering SX Forms Post Type
        register_post_type( $this->post_type, $args );

    }

    /**
     * Get all charities
     *
     * @param array $args
     * @return array|bool
     */
    public function sx_charities_get_posts( $args = [] ){
        // Set up the query parameters
        $default = array(
            'post_type'       => $this->post_type,
            'posts_per_page'  => -1,
        );
        $args    = array_diff( $default, $args );
        $results = [];

        // Perform the query
        $my_query = new WP_Query( $args );

        if ( empty( $my_query ) || is_wp_error( $my_query ) ) {
            return false;
        }

        // Process how post will be displayed
        if ( $my_query->have_posts() ) {
            while ( $my_query->have_posts() ) {
                $my_query->the_post();
                $post = get_post();
                $results[$post->ID] = get_the_title( $post );
            }
        }

        return $results;
    }

    /**
     * Check if charity exists by it's ID
     *
     * @param $id
     * @return bool
     */
    public function sx_charity_exist( $id ){
        $exist = false;

        $result = $this->sx_charities_get_posts( ['p' => intval( $id )] );

        if ( !empty( $result ) && !empty( $result[$id] ) ) {
            $exist = true;
        }

        return $exist;
    }


}
