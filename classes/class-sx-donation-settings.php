<?php
/**
 * Class responsible for all Settings in SX Donations plugin
 */

/**
 * Class SX_Donation_Settings
 */
class SX_Donation_Settings extends SX_Main {

    public $show_in_menu;
    public $current_lang;
    public $default_active_tab;
    public $settings_slug = 'sx_donations_settings';
    public $validation_messages;
    public $form_labels;
    public $makecommerce_fields;
    public $tabs = [
        ["slug" => 'val_messages', "label" => "Validation Messages"],
        ["slug" => 'makecommerce_settings', "label" => "Makecommerce Settings"],
        ["slug" => 'form_labels', "label" => "Form Field Labels"],
    ];

    public function __construct( $args, $show_in_menu = true ) {
        parent::__construct( $args );

        $this->default_active_tab  = $this->tabs[0]['slug'];
        $this->show_in_menu        = $show_in_menu;
        $this->validation_messages = [
            "first_name" => ["key" => "_first_name_error", "label" => __( "First Name Error", $this->textdomain )],
            "last_name"  => ["key" => "_last_name_error", "label" => __( "Last Name Error", $this->textdomain )],
            "ssn"        => ["key" => "_ssn_error", "label" => __( "SSN Error", $this->textdomain )],
            "sum"        => ["key" => "_sum_error", "label" => __( "Sum Error", $this->textdomain )],
            "charity"    => ["key" => "_charity_error", "label" => __( "Charity Error", $this->textdomain )],
            "checkbox"   => ["key" => "_checkbox_error", "label" => __( "Checkbox Error", $this->textdomain )],
            "donation_message_error" => ["key" => "_donation_message_error", "label" => __( "Donation error", $this->textdomain )],
            "donation_message_ok" => ["key" => "_donation_message_ok", "label" => __( "Donation OK", $this->textdomain )],
            "donation_message_successful" => ["key" => "_donation_message_successful", "label" => __( "Donation message successful", $this->textdomain )],
            "donation_message_cancelled" => ["key" => "_donation_message_cancelled", "label" => __( "Donation message cancelled", $this->textdomain )],
            "donation_message_failed" => ["key" => "_donation_message_failed", "label" => __( "Donation message failed", $this->textdomain )],
        ];
        $this->form_labels = [
            "first_name" => ["key" => "_first_name_label", "label" => __( "First Name", $this->textdomain )],
            "last_name"  => ["key" => "_last_name_label", "label" => __( "Last Name", $this->textdomain )],
            "ssn"        => ["key" => "_ssn_label", "label" => __( "SSN", $this->textdomain )],
            "sum"        => ["key" => "_sum_label", "label" => __( "Sum", $this->textdomain )],
            "charity"    => ["key" => "_charity_label", "label" => __( "Charity", $this->textdomain )],
            "pg"         => ["key" => "_pg_label", "label" => __( "Payment Gateway", $this->textdomain )],
            "checkbox"   => ["key" => "_checkbox_label", "label" => __( "Checkbox", $this->textdomain )],
            "submit"     => ["key" => "_submit_label", "label" => __( "Submit Button", $this->textdomain )],
        ];
        $this->makecommerce_fields = [
            "live" => [
                "shop_id" => [
                    "key"   => "sxmc_shop_id",
                    "label" => __("Shop ID", $this->textdomain),
                    "type"  => "text",
                ],
                "public_key" => [
                    "key"   => "sxmc_public_key",
                    "label" => __("Public Key", $this->textdomain),
                    "type"  => "text",
                ],
                "secret_key" => [
                    "key"   => "sxmc_secret_key",
                    "label" => __("Secret Key", $this->textdomain),
                    "type"  => "text",
                ],
//                "success_url" => [
//                    "key"   => "sxmc_success_url",
//                    "label" => __("Successful landing url", $this->textdomain),
//                    "type"  => "text",
//                ],
            ],
            "sandbox" => [
                "key"   => "sxmc_sandbox_mode",
                "label" => __("Sanbox Mode", $this->textdomain),
                "type"  => "checkbox",
            ],
            "test" => [
                "notice"  => [__( "If sandbox is enabled the plugin will use credentials specifically for sandbox mode:", $this->textdomain )],
                "shop_id" => [
                    "key"   => "sxmc_sandbox_shop_id",
                    "label" => __("Sanbox Shop ID", $this->textdomain),
                    "type"  => "text",
                ],
                "public_key" => [
                    "key"   => "sxmc_sandbox_public_key",
                    "label" => __("Sanbox Public Key", $this->textdomain),
                    "type"  => "text",
                ],
                "secret_key" => [
                    "key"   => "sxmc_sandbox_secret_key",
                    "label" => __("Sanbox Secret Key", $this->textdomain),
                    "type"  => "text",
                ],
//                "success_url" => [
//                    "key"   => "sxmc_sandbox_success_url",
//                    "label" => __("Sanbox Successful landing url", $this->textdomain),
//                    "type"  => "text",
//                ],
            ],
        ];

        // Register settings page for SX Donations plugin
        add_action( 'admin_menu', array( $this, 'sx_donation_settings_page' ) );
    }

    /**
     * Get current page language
     *
     * @return string
     */
    public function get_current_wpml_language(){
        $lang = apply_filters( 'wpml_current_language', NULL );

        if ( empty( $lang ) ) {
            $lang = ( defined( 'ICL_LANGUAGE_CODE' ) ) ? ICL_LANGUAGE_CODE : '';
        }

        return $lang;
    }

    /**
     * Add settings page for SX Donations plugin
     */
    public function sx_donation_settings_page(){
        add_submenu_page(
            $this->show_in_menu,
            __( 'Donations Settings', $this->textdomain ),
            __( 'Settings', $this->textdomain ),
            'manage_options',
            $this->settings_slug,
            array( $this, 'sx_donation_settings_page_content' )
        );
    }

    /**
     * Page content for SX Donations Settings
     */
    public function sx_donation_settings_page_content(){

        if ( !current_user_can( 'manage_options' ) ) {
            return;
        }

        $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : $this->default_active_tab;

        $this->save_admin_settings( $_POST );

        do_action('admin_notices');

        ?>
        <div class="wrap">
            <h1 class="wp-heading-inline"><?php echo esc_html( get_admin_page_title() ); ?></h1>

            <p><?php _e( "Form Shortcode: ",  $this->textdomain ); ?><code>[<?php echo $this->shortcode; ?>]</code></p>
            <p><?php _e( "Return, Cancel, Notifications URL: ",  $this->textdomain ); ?><code><?php echo site_url() . '/' . $this->endpoint; ?></code></p>

            <?php $this->render_admin_settings_tabs( $active_tab ); ?>

            <form action="" method="post">
                <div id="poststuff">
                    <div class="metabox-holder columns-2">
                        <div id="submitdiv" class="postbox">
                            <div class="inside">

                                <table class="form-table ct-metabox">
                                    <tbody>
                                        <?php

                                        switch ( $active_tab ) {
                                            case $this->tabs[1]["slug"]:
                                                $this->render_admin_makecommerce_settings();
                                                break;
                                            case $this->tabs[2]["slug"]:
                                                $this->render_admin_fields_form_labels();
                                                break;
                                            case $this->tabs[0]["slug"]:
                                            default:
                                                $this->render_admin_fields_validation_messages();
                                        }

                                        ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div><!-- /post-body -->
                    <input type="hidden" name="active_tab" value="<?php echo $active_tab; ?>" />
                    <?php submit_button( 'Save' ); ?>
                    <br class="clear">
                </div><!-- /poststuff -->
            </form>
        </div>

        <style>
            <?php
                if ( !$this->get_setting_value('sxmc_sandbox_mode') ) {
                    echo 'tr.sandbox{display:none;}';
                }
            ?>
            tr.ct-field th {
                padding-left: 15px;
            }

            .form-table tr.ct-field:last-child>th,
            .form-table tr.ct-field:last-child>td {
                border-bottom: none
            }

            .ct-metabox tr.ct-field {
                border-top: 1px solid #ececec
            }

            .ct-metabox tr.ct-field:first-child {
                border-top: 0
            }

            .italic{
                font-style: italic;
            }

            tr.ct-field textarea {
                height: 100px;
            }
            div.postbox {
                width: 60%;
            }
            @media screen and (max-width: 782px) {
                .form-table td input,
                .form-table td select {
                    margin: 0
                }
                .ct-metabox tr.ct-field>th {
                    padding-top: 15px
                }
                .ct-metabox tr.ct-field>td {
                    padding-bottom: 15px
                }
            }
        </style>

        <script>
            (function($){
                $('#<?php echo $this->makecommerce_fields['sandbox']['key']; ?>').on("change", function(){
                    if ( $(this).attr('checked') ) {
                        $('tr.sandbox').show();
                    } else {
                        $('tr.sandbox').hide();
                    }
                });
            })(jQuery);
        </script>
        <?php
    }

    /**
     * Form up tabs html block for settings page
     *
     * @param $active_tab
     */
    private function render_admin_settings_tabs( $active_tab ){
        $tabs_html = '<h2 class="nav-tab-wrapper">';

        foreach ( $this->tabs as $tab ) {
            $active = ( $tab['slug'] == $active_tab ) ? ' nav-tab-active' : '';
            $tabs_html .= '<a href="'.$this->show_in_menu.'&page='.$this->settings_slug.'&tab='.$tab['slug'].'" class="nav-tab'.$active.'">'.__( $tab["label"], $this->textdomain ).'</a>';
        }

        $tabs_html .= '</h2>';

        echo $tabs_html;
    }

    /**
     * Form up html for SX Donations plugin validation messages in admin settings area
     *
     * @return string
     */
    public function render_admin_fields_form_labels(){
        $lang = $this->get_current_wpml_language();

        foreach ( $this->form_labels as $k => $field ) {
            $key   = $lang . $field["key"];
            $value = $this->get_setting_value( $key );
            echo '<tr class="form-field ct-field">';
            echo '<th scope="row" valign="top">';
            echo '<label for="'.$key.'">'. $field["label"] .'</label>';
            echo '</th><td><div>';
            if ( $k == "checkbox" ) {
                $args = array (
                    'tinymce' => true,
                    'quicktags' => true,
                );
                wp_editor( $value, $key, $args );
            } else {
                echo '<textarea name="'.$key.'" id="'.$key.'" class="regular-text" tabindex="2">'.$value.'</textarea>';
            }
            echo '</div></td></tr>';
        }

    }

    /**
     * Form up html for SX Donations plugin validation messages in admin settings area
     *
     * @return string
     */
    public function render_admin_fields_validation_messages(){
        $html = '';
        $lang = $this->get_current_wpml_language();

        foreach ( $this->validation_messages as $message ) {
            $key   = $lang . $message["key"];
            $value = $this->get_setting_value( $key );
            $html .= '<tr class="form-field ct-field">';
            $html .= '<th scope="row" valign="top">';
            $html .= '<label for="'.$key.'">'. $message["label"] .'</label>';
            $html .= '</th><td><div>';
            $html .= '<textarea name="'.$key.'" id="'.$key.'" class="regular-text" tabindex="2">'.$value.'</textarea>';
            $html .= '</div></td></tr>';
        }

        echo $html;
    }

    /**
     * Form up html row for each individual settings field passed to this function
     *
     * @param $key
     * @param $value
     * @param string $class
     * @return string
     */
    public function render_admin_settings_field( $key, $value, $class = '' ){
        $html  = '<tr class="form-field ct-field'.$class.'">';

        if ( $key == 'notice' ) {
            $html .= '<td colspan="2">';
            $html .= '<p class="italic">'.$value[0].'</p>';
            $html .= '</td>';
        } else {
            $option = $this->get_setting_value( $value["key"], false );
            $html .= '<th scope="row" valign="top">';
            $html .= '<label for="'.$value["key"].'">'.$value["label"].'</label>';
            $html .= '</th><td><div>';
            switch ( $value["type"] ) {
                case 'text':
                    $html .= '<input type="text" id="'.$value["key"].'" name="'.$value["key"].'" value="'.$option.'"/>';
                    break;
                case 'checkbox':
                    $checked = ( $option ) ? 'checked="checked" ' : '';
                    $html .= '<input type="hidden" name="'.$value["key"].'" value="0" '.$checked.'/>';
                    $html .= '<input type="checkbox" id="'.$value["key"].'" name="'.$value["key"].'" value="1" '.$checked.'/>';
                    break;
            }

            $html .= '</div></td>';
        }
        $html .= '</tr>';

        return $html;
    }

    /**
     * Form up settings section for MakeCommerce integration
     *
     * @return string
     */
    public function render_admin_makecommerce_settings(){
        $html = '';
        $skip = [];

        foreach ( $this->makecommerce_fields as $group => $mc_field ) {
            $class = ( $group == 'test' ) ? ' sandbox' : '';

            foreach ( $mc_field as $name => $field ) {
                if ( is_array( $field ) ) {
                    $html .= $this->render_admin_settings_field( $name, $field, $class );
                } else {
                    if ( in_array( $group, $skip ) ) { continue; }
                    $html .= $this->render_admin_settings_field( $group, $mc_field, $class );
                    $skip[] = $group;
                }
            }

        }

        echo $html;
    }

    /**
     * Save settings in admin area
     *
     * @param $values
     */
    public function save_admin_settings( $values ){
        $updated = [];

        switch ( $values["active_tab"] ) {
            case $this->tabs[1]["slug"]:
                $skip = [];
                foreach ( $this->makecommerce_fields as $group => $mc_field ) {
                    foreach ( $mc_field as $name => $field ) {
                        if ( is_array( $field ) ) {
                            if ( isset( $values[$field['key']] ) ) {
                                $updated[] = $this->save_settings_value( $field['key'], $values[$field['key']] );
                            }
                        } else {
                            if ( in_array( $group, $skip ) ) { continue; }
                            if ( isset( $values[$mc_field['key']] ) ) {
                                $updated[] = $this->save_settings_value( $mc_field['key'], $values[$mc_field['key']] );
                            }
                            $skip[] = $group;
                        }
                    }

                }
                do_action( "after_save_makecommerce_creedentials" );
            break;
            case $this->tabs[2]["slug"]:
                foreach ( $this->form_labels as $field ) {
                    $option = $this->get_current_wpml_language() . $field["key"];

                    if ( isset( $values[$option] ) ) {
                        $updated[] = $this->save_settings_value( $option, $values[$option] );
                    }
                }
            break;
            case $this->tabs[0]["slug"]:
            default:
                foreach ( $this->validation_messages as $message ) {
                    $option = $this->get_current_wpml_language() . $message["key"];

                    if ( isset( $values[$option] ) ) {
                        $updated[] = $this->save_settings_value( $option, $values[$option] );
                    }
                }
        }

        if ( in_array( true, $updated ) ) {
            add_action('admin_notices', function () {
                echo '<div class="updated notice"><p>' . __("Settings saved", $this->textdomain) . '</p></div>';
            });
        }
    }

    /**
     * Get value of setting option by key
     * if $wpml is true it will look for value in current language first
     *
     * @param $key
     * @param bool $wpml
     * @return mixed|string
     */
    public function get_setting_value( $key, $wpml = true ){
        $option = '';
        // Get option based on current wpml language
        if ( $wpml ) {
            $lang = ( !empty( $_POST["lang"] ) ) ? $_POST["lang"] : $this->get_current_wpml_language();
            $option = get_option( $lang . $key, '' );
        }
        // If $wmpl == false or if setting is not set for selected language
        if ( empty( $option ) ) {
            $option = get_option( $key, '' );
        }

        return html_entity_decode( $option );
    }

    /**
     * Save individual setting value
     *
     * @param $key
     * @param $value
     * @return bool
     */
    public function save_settings_value( $key, $value ) {
        return update_option( $key, htmlentities( stripslashes( $value ) ) );
    }

    /**
     * Check what mode is plugin currently on - testing(true) or live(false)
     *
     * @return mixed|string
     */
    public function is_sandbox_mode(){
        return $this->get_setting_value( $this->makecommerce_fields['sandbox']['key'], false );
    }

    /**
     * Get credentials to use in requests to MakeCommerce API
     * Testing or live, based on which mode is on
     *
     * @param $credential
     * @return mixed|string
     */
    public function get_makecommerce_credential( $credential ){
        $env = ( $this->is_sandbox_mode() ) ? 'test' : 'live';

        return $this->get_setting_value( $this->makecommerce_fields[$env][$credential]['key'], false );
    }

    /**
     * Get form field validation error message
     *
     * @param $field
     * @return mixed
     */
    public function get_form_validation_error_message( $field ){
        return $this->get_setting_value( $this->validation_messages[$field]["key"] );
    }

}
