<?php
/**
 * Global plugin settings
 */

/**
 * Class SX_Main
 */
class SX_Main {
    public $textdomain;
    public $post_types;
    public $shortcode;
    public $endpoint;

    /**
     * SX_Main constructor.
     * @param $args
     */
    public function __construct( $args ) {
        if ( !empty( $args["textdomain"] ) ) $this->textdomain = $args["textdomain"];
        if ( !empty( $args["post_types"] ) ) $this->post_types = $args["post_types"];
        if ( !empty( $args["shortcode"] ) ) $this->shortcode = $args["shortcode"];
        if ( !empty( $args["endpoint"] ) ) $this->endpoint = $args["endpoint"];

        add_action( 'init', array( $this, 'sx_main_maybe_start_session' ), 1);
        add_action( 'wp_logout', array( $this, 'sx_main_end_session' ) );
        add_action( 'wp_login', array( $this, 'sx_main_end_session' ) );
    }

    function sx_main_maybe_start_session() {
        if( !session_id() ) {
            session_start();
        }
    }

    function sx_main_end_session() {
        if( !session_id() ) {
            session_destroy();
        }
    }

    /**
     * Get real user IP
     *
     * @return string
     */
    public function get_user_real_ip(){
        $ip = '';
        if ( !empty( $_SERVER["HTTP_X_FORWARDED_FOR"] ) ) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else if ( !empty( $_SERVER["HTTP_X_REAL_IP"] ) ) {
            $ip = $_SERVER["HTTP_X_REAL_IP"];
        } else if ( !empty( $_SERVER["REMOTE_ADDR"] ) ) {
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }
}
