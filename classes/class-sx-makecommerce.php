<?php
/**
 * Class provides connection with MakeCommerce API using their library
 */

require_once WP_PLUGIN_DIR . '/sx-donations/libs/maksekeskus-1.3/vendor/autoload.php';
use Maksekeskus\Maksekeskus;

/**
 * Class SX_Makecommerce
 */
class SX_Makecommerce {

    public $settings;
    public $maksekeskus;
    public $secret_key;
    public $public_key;
    public $shop_id;
    public $is_test;

    public function __construct( $settings )
    {
        // Plugin settings options and methods
        $this->settings    = $settings;
        // Get MakeCommerce credentials from saved settings
        $this->is_test     = $this->settings->is_sandbox_mode();
        $this->shop_id     = $this->settings->get_makecommerce_credential('shop_id');
        $this->public_key  = $this->settings->get_makecommerce_credential('public_key');
        $this->secret_key  = $this->settings->get_makecommerce_credential('secret_key');
        // Construct MakeCommerce API connection
        $this->maksekeskus = new Maksekeskus( $this->shop_id, $this->public_key, $this->secret_key, $this->is_test );
        // Create endpoint if does not exist
        add_action( "init", array( $this, "maybe_create_endpoint" ) );
        // Add function to register event to WordPress init
        add_action( "fetch_and_save_payment_gateways", array( $this, 'fetch_and_save_payment_gateways' ) );
        add_action( 'init', array( $this, 'recurring_update_of_payment_methods') );
        // Update payment methods on saving MakeCommerce credentials
        add_action( "after_save_makecommerce_creedentials", array( $this, "fetch_and_save_payment_gateways" ) );
    }

    /**
     * Cron schedule payment_gateways update
     */
    public function recurring_update_of_payment_methods() {
        // Make sure this event hasn't been scheduled
        if( !wp_next_scheduled( 'fetch_and_save_payment_gateways' ) ) {
            // Schedule the event
            wp_schedule_event( time(), 10*MINUTE_IN_SECONDS, "fetch_and_save_payment_gateways" );
        }
    }

    /**
     * Make request call to Maksekeskus API and get available payment methods for provided options
     *
     * @return \Maksekeskus\obj
     */
    public function get_payment_methods(){
        // temporary hardcode currency and country settings
        $context["currency"] = "eur";
        $context["country"]  = "ee";

        $pay_methods = $this->maksekeskus->getPaymentMethods( $context );

        return $pay_methods;
    }

    /**
     * Get currently available in MakeCommerce payment methods
     */
    public function fetch_and_save_payment_gateways(){
        $pgs = $this->get_payment_methods();

        if ( !empty( $pgs->banklinks ) ) {
            update_option( 'payment_gateways', json_encode( $pgs->banklinks ) );
        }
    }

    /**
     * Get payment methods
     *
     * @return string|object
     */
    public function get_saved_payment_methods(){
        $response = '';
        $pg = get_option( 'payment_gateways' );

        if ( !empty( $pg ) ) {
            $response = json_decode( $pg );
        }

        return $response;
    }

    /**
     * Create endpoint for MakeCommerce API callbacks
     */
    public function maybe_create_endpoint(){
        // Do nothing if page already exists
        if ( get_page_by_path( $this->settings->endpoint , OBJECT ) ) {
            return;
        }
        // Set page parameters
        $page_details = array(
            'post_title'    => 'SX Donations Endpoint',
            'post_content'  => '[donation_result]',
            'post_status'   => 'publish',
            'post_type'     => 'page',
            'post_name'     => $this->settings->endpoint,
        );
        // Create page
        wp_insert_post( $page_details );
    }

}
