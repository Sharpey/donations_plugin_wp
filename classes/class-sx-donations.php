<?php
/**
 * Main plugin class
 */

/**
 * Class SX_Donations
 */
class SX_Donations extends SX_Main {

    public $charities;
    public $settings;
    public $sxmc;
    public $post_type;
    public $meta_fields = [
        "first_name"  => "First Name", // person's first name
        "last_name"   => "Last Name", // person's last name
        "ssn"         => "SSN", // person's social security number
        "sum"         => "Sum", // donated sum
        "charity"     => "Charity", // ID of selected sx charity
        "status"      => "Status", // pending / successful / cancelled / failed
        "testing"     => "Payment mode", // True if donation is a test or False if real
        "transaction" => "Transaction ID", // Get from in response to MakeCommerce API request
    ];
    public $donation_statuses = [
        "pending",
        "successful",
        "cancelled",
        "failed",
    ];
    public $default_status;

    /**
     * SX_Donations constructor.
     * @param $args
     */
    public function __construct( $args ) {
        // Set default status for donation post
        $this->default_status = $this->donation_statuses[0];
        // Run parent constructor to set global plugin parameters
        parent::__construct( $args );
        // Set this class's post type
        if ( !empty( $this->post_types["donation"] ) ) $this->post_type = $this->post_types["donation"];
        // Register SX Charities Post Type
        add_action( 'init', array( $this, 'sx_donations_post_type' ) );
        // Hook to remove all links from row-actions for donations post type
        add_filter( 'post_row_actions', array( $this, 'sx_donations_remove_all_actions'), 9999, 1 );
        // Manage donations columns
        add_filter( 'manage_'.$this->post_type.'_posts_columns', array( $this, 'sx_donations_manage_columns' ), 9999, 1 );
        // Output content in added columns
        add_action('manage_'.$this->post_type.'_posts_custom_column', array( $this, 'sx_donations_columns_content'), 9999, 2 );
        // Initiate charities post type
        $this->charities = new SX_Charities( $args, 'edit.php?post_type=' . $this->post_type );
        // Initiate plugin settings
        $this->settings = new SX_Donation_Settings( $args, 'edit.php?post_type=' . $this->post_type );
        // Initiate connection with MakeCommerce
        $this->sxmc = new SX_Makecommerce( $this->settings );
        // Shortcode that outputs a form to accept donations
        add_shortcode( $this->shortcode, array( $this, 'sx_donations_form' ) );
        // Catch and process all the requests to MakeCommerce endpoint
        add_action( "init", array( $this, "sxmc_endpoint" ) );
        add_shortcode( 'donation_result', array( $this, "donation_result" ) );
    }

    /**
     * Set parameters for and register sx_donation post type
     */
    public function sx_donations_post_type() {

        // Set UI labels for SX Charities Post Type
        $labels = array(
            'name'                => _x( 'Donations', 'Post Type General Name', $this->textdomain ),
            'singular_name'       => _x( 'Donation', 'Post Type Singular Name', $this->textdomain ),
            'menu_name'           => __( 'SX Donations', $this->textdomain ),
            'parent_item_colon'   => __( 'Parent Donation', $this->textdomain ),
            'all_items'           => __( 'Donations', $this->textdomain ),
            'view_item'           => __( 'View Donation', $this->textdomain ),
            'add_new_item'        => __( 'Add New Donation', $this->textdomain ),
            'add_new'             => __( 'Add New', $this->textdomain ),
            'edit_item'           => __( 'Edit Donation', $this->textdomain ),
            'update_item'         => __( 'Update Donation', $this->textdomain ),
            'search_items'        => __( 'Search Donations', $this->textdomain ),
            'not_found'           => __( 'Not Found', $this->textdomain ),
            'not_found_in_trash'  => __( 'Not found in Trash', $this->textdomain ),
        );

        $supports = array(
            'title',
        );

        // Set other options for SX Charities Post Type
        $args = array(
            'description'         => __( 'SX Donations', $this->textdomain ),
            'labels'              => $labels,
            'supports'            => $supports,
            'taxonomies'          => array(),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 11,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => false, // disable view
            'map_meta_cap'        => false, // disable edit/delete
            'capability_type'     => 'post',
            'capabilities'        => array(
                'create_posts' => 'do_not_allow', // false for < WP 4.5;
            ),
        );

        // Registering SX Forms Post Type
        register_post_type( $this->post_type, $args );

    }

    /**
     * Remove actions links in Donations view
     *
     * @param $actions
     * @return array
     */
    public function sx_donations_remove_all_actions( $actions ) {
        return [];
    }

    /**
     * Insert donation into DB
     *
     * @param array $data
     * @return bool
     */
    public function sx_donations_create_post( $data = [] ){
        $post_id = wp_insert_post(['post_type' => $this->post_type, 'post_title' => '', 'post_status' => 'publish',]);

        if ( empty( $post_id ) ) {
            return false;
        }
        // Add donation data to a donation post as meta values
        foreach ( $this->meta_fields as $meta_key => $label ) {
            update_post_meta( $post_id, $meta_key, $data[$meta_key] );
        }
        // Set default status
        update_post_meta( $post_id, "status", $this->default_status );

        return true;
    }

    /**
     * Manage columns in donations view
     *
     * @param $defaults
     * @return array
     */
    public function sx_donations_manage_columns( $defaults ) {
        $columns = ["id" => __( '#', $this->textdomain )];

        foreach ( $this->meta_fields as $meta_key => $label ) {
            if ( $meta_key != 'transaction' ) {
                $columns[$meta_key] = __( $label, $this->textdomain );
            }
        }

        $columns["date_time"] = __( "Date", $this->textdomain );

        return $columns;
    }

    /**
     * Output values for new columns in donations view table
     *
     * @param $column_name
     * @param $post_ID
     */
    public function sx_donations_columns_content( $column_name, $post_ID ) {

        switch ( $column_name ) {
            case 'id':
                echo $post_ID;
                break;
            case 'date_time':
                echo get_the_date( "d/m/Y H:i:s" );
                break;
            case 'charity':
                echo get_the_title( get_post_meta( $post_ID, $column_name, true ) );
                break;
            case 'testing':
                $testing = get_post_meta( $post_ID, $column_name, true );
                $value   = ( $testing ) ? "testing" : "production";
                echo __( $value, $this->textdomain );
                break;
            default:
                echo get_post_meta( $post_ID, $column_name, true );
        }

    }

    /**
     * Generate form to receive donations through
     *
     * @param $args
     * @return string
     */
    public function sx_donations_form( $args ){
        // Check if user has unfinished donation form values
        $first_name = ( !empty($_SESSION["form_values"]["first_name"]) ) ? $_SESSION["form_values"]["first_name"] : '';
        $last_name  = ( !empty($_SESSION["form_values"]["last_name"]) ) ? $_SESSION["form_values"]["last_name"] : '';
        $ssn        = ( !empty($_SESSION["form_values"]["ssn"]) ) ? $_SESSION["form_values"]["ssn"] : '';
        $sum        = ( !empty($_SESSION["form_values"]["sum"]) ) ? $_SESSION["form_values"]["sum"] : '';
        $charity    = ( !empty($_SESSION["form_values"]["charity"]) ) ? $_SESSION["form_values"]["charity"] : '';
        $sel_pg     = ( !empty($_SESSION["form_values"]["payment_gateway"]) ) ? $_SESSION["form_values"]["payment_gateway"] : '';
        $checkbox   = ( !empty($_SESSION["form_values"]["accept"]) ) ? $_SESSION["form_values"]["accept"] : '';
        $checkbox_checked = ( !empty( $checkbox ) ) ? ' checked="checked" ' : '';
        // Get form field labels
        $first_name_label = $this->settings->get_setting_value( '_first_name_label' );
        $first_name_label = ( !empty( $first_name_label ) ) ? $first_name_label : __( "First Name", $this->textdomain );
        $last_name_label = $this->settings->get_setting_value( '_last_name_label' );
        $last_name_label = ( !empty( $last_name_label ) ) ? $last_name_label : __( "Last Name", $this->textdomain );
        $ssn_label = $this->settings->get_setting_value( '_ssn_label' );
        $ssn_label = ( !empty( $ssn_label ) ) ? $ssn_label : __( "SSN", $this->textdomain );
        $sum_label = $this->settings->get_setting_value( '_sum_label' );
        $sum_label = ( !empty( $sum_label ) ) ? $sum_label : __( "Sum", $this->textdomain );
        $charity_label = $this->settings->get_setting_value( '_charity_label' );
        $charity_label = ( !empty( $charity_label ) ) ? $charity_label : __( "Charity", $this->textdomain );
        $pg_label = $this->settings->get_setting_value( '_pg_label' );
        $pg_label = ( !empty( $pg_label ) ) ? $pg_label : __( "Payment Gateway", $this->textdomain );
        $submit_label = $this->settings->get_setting_value( '_submit_label' );
        $submit_label = ( !empty( $submit_label ) ) ? $submit_label : __( "Donate", $this->textdomain );
        $checkbox_label = $this->settings->get_setting_value( '_checkbox_label' );
        $checkbox_label = ( !empty( $checkbox_label ) ) ? $checkbox_label : __( "Accept terms and conditions", $this->textdomain );
        // Check if validation messages are present
        if ( !empty( $_SESSION["form_values"]["messages"]["first_name"] ) ) {
            $first_name_error = $_SESSION["form_values"]["messages"]["first_name"];
            $first_name_error_class = ' error';
        } else {
            $first_name_error = '';
            $first_name_error_class = '';
        }

        if ( !empty( $_SESSION["form_values"]["messages"]["last_name"] ) ) {
            $last_name_error = $_SESSION["form_values"]["messages"]["last_name"];
            $last_name_error_class = ' error';
        } else {
            $last_name_error = '';
            $last_name_error_class = '';
        }

        if ( !empty( $_SESSION["form_values"]["messages"]["ssn"] ) ) {
            $ssn_error = $_SESSION["form_values"]["messages"]["ssn"];
            $ssn_error_class = ' error';
        } else {
            $ssn_error = '';
            $ssn_error_class = '';
        }

        if ( !empty( $_SESSION["form_values"]["messages"]["sum"] ) ) {
            $sum_error = $_SESSION["form_values"]["messages"]["sum"];
            $sum_error_class = ' error';
        } else {
            $sum_error = '';
            $sum_error_class = '';
        }

        if ( !empty( $_SESSION["form_values"]["messages"]["charity"] ) ) {
            $charity_error = $_SESSION["form_values"]["messages"]["charity"];
            $charity_error_class = ' error';
        } else {
            $charity_error = '';
            $charity_error_class = '';
        }

        if ( !empty( $_SESSION["form_values"]["messages"]["payment_gateway"] ) ) {
            $pg_error = $_SESSION["form_values"]["messages"]["payment_gateway"];
            $pg_error_class = ' error';
        } else {
            $pg_error = '';
            $pg_error_class = '';
        }

        if ( !empty( $_SESSION["form_values"]["messages"]["checkbox"] ) ) {
            $checkbox_error = $_SESSION["form_values"]["messages"]["checkbox"];
            $checkbox_error_class = ' error';
        } else {
            $checkbox_error = '';
            $checkbox_error_class = '';
        }

        // Get all available charities
        $charities = $this->charities->sx_charities_get_posts();
        // Form a select field with charities
        $sel_char  = '<label for="charity" class="donations-label required">' . $charity_label . '</label>';
        $sel_char .= '<select id="charity" name="charity" class="donations-select required'.$charity_error_class.'" required>';
        foreach ( $charities as $charity_id => $charity_name  ) {
            $selected = ( $charity_id == $charity ) ? " selected='selected' " : "";
            $sel_char .= '<option value="'.$charity_id.'"'.$selected.'>'.__( $charity_name, $this->textdomain ).'</option>';
        }
        $sel_char .= '</select>';
        $sel_char .= '<p class="donations-message error" id="charity_error">'.$charity_error.'</p>';

        $payment_gateways = $this->sxmc->get_saved_payment_methods();

        // Form a select field with payment methods available for user or output message if no methods available
        if ( !empty( $payment_gateways ) ) {
            $pm_select  = '<label for="payment_gateway" class="donations-label required">' . $pg_label . '</label>';
            $pm_select .= '<select id="payment_gateway" name="payment_gateway" class="donations-select required'.$pg_error_class.'" required>';
            foreach ( $payment_gateways as $banklink ) {
                $selected = ( $banklink->url == $sel_pg ) ? " selected='selected' " : "";
                $pm_select .= '<option value="'.$banklink->url.'"'.$selected.'>'.ucfirst( $banklink->name ).'</option>';
            }
            $pm_select .= '</select>';
            $pm_select .= '<p class="donations-message error" id="payment_gateway_error">'.$pg_error.'</p>';
        } else {
            $pm_select = '<p>'.__( "Sorry, no payment gateways available at the moment. Please, try again later.", $this->textdomain ).'</p>';
        }

        // Construct form html
        $form  = '<div class="donations-form-wrap">';
        $form .= '<form action="'.site_url().'/'.$this->settings->endpoint.'" id="sx_donations_form" class="donations-form" method="post">';
        $form .= '<label for="first_name" class="donations-label required">' . $first_name_label . '</label>';
        $form .= '<input type="text" class="donations-input required'.$first_name_error_class.'" id="first_name" name="first_name" value="'.$first_name.'" required />';
        $form .= '<p class="donations-message error" id="first_name_error">'.$first_name_error.'</p>';
        $form .= '<label for="last_name" class="donations-label required">' . $last_name_label . '</label>';
        $form .= '<input type="text" class="donations-input required'.$last_name_error_class.'" id="last_name" name="last_name" value="'.$last_name.'" required />';
        $form .= '<p class="donations-message error" id="last_name_error">'.$last_name_error.'</p>';
        $form .= '<label for="ssn" class="donations-input required">' . $ssn_label . '</label>';
        $form .= '<input type="text" class="donations-label required'.$ssn_error_class.'" id="ssn" name="ssn" value="'.$ssn.'" required />';
        $form .= '<p class="donations-message error" id="ssn_error">'.$ssn_error.'</p>';
        $form .= $sel_char;
        $form .= '<label for="sum" class="donations-label required">' . $sum_label . '</label>';
        $form .= '<input type="text" class="donations-input required'.$sum_error_class.'" id="sum" name="sum" value="'.$sum.'" required />';
        $form .= '<p class="donations-message error" id="sum_error">'.$sum_error.'</p>';
        $form .= $pm_select;
        $form .= '<input type="hidden" name="accept" value="0" />';
        $form .= '<label for="accept" class="donations-label chekbox-label required"><input type="checkbox" class="donations-input checkbox'.$checkbox_error_class.'" id="accept" name="accept"'.$checkbox_checked.' value="1" />' . $checkbox_label . '</label>';
        $form .= '<p class="donations-message error" id="checkbox_error">'.$checkbox_error.'</p>';
        $form .= '<input type="hidden" name="action" value="user_donate" />';
        $form .= '<input type="hidden" name="lang" value="'.$this->settings->get_current_wpml_language().'" />';
        $form .= '<input type="submit" value="' . $submit_label . '" />';
        $form .= '</form>';
        $form .= '</div>';

        return $form;
    }

    /**
     * Check if submitted donation form values are valid.
     * Return json encoded array with boolean error and array messages
     *
     * @param array $values
     * @param bool $json
     * @return mixed|string
     */
    public function sx_validate_fields( $values, $json = false ){
        $error       = false;
        $messages    = [];
        $name_regexp = '/^[\p{L}]+\.?\s?\'?-?[\p{L}]+\.?\'?$/i';
        $ssn_regexp  = '/^\d{11}$/';
        $sum_regexp  = '/^\d+\.?\d{0,2}$/';

        if ( empty( $values["first_name"] ) || !preg_match( $name_regexp, $values["first_name"] ) ) {
            $error = true;
            $messages["first_name"] = $this->settings->get_form_validation_error_message("first_name");
        }

        if ( empty( $values["last_name"] ) || !preg_match( $name_regexp, $values["last_name"] ) ) {
            $error = true;
            $messages["last_name"] = $this->settings->get_form_validation_error_message("last_name");
        }

        if ( empty( $values["ssn"] ) || !preg_match( $ssn_regexp, $values["ssn"] ) ) {
            $error = true;
            $messages["ssn"] = $this->settings->get_form_validation_error_message("ssn");
        }

        if ( empty( $values["sum"] ) || !preg_match( $sum_regexp, $values["sum"] ) ) {
            $error = true;
            $messages["sum"] = $this->settings->get_form_validation_error_message("sum");
        }

        if ( empty( $values["charity"] ) || !$this->charities->sx_charity_exist( $values["charity"] ) ) {
            $error = true;
            $messages["charity"] = $this->settings->get_form_validation_error_message("charity");
        }

        if ( empty( $values["accept"] ) ) {
            $error = true;
            $messages["checkbox"] = $this->settings->get_form_validation_error_message("checkbox");
        }

        $response = ["error" => $error, "messages" => $messages];

        if ( $json ) {$response = json_encode( $response );}

        return $response;
    }

    /**
     * Function serves as an endpoint for MakeCommerce integration
     */
    public function sxmc_endpoint(){
        if ( trim( strtok( $_SERVER["REQUEST_URI"], '?' ), '/' ) == $this->settings->endpoint ) {
            // Process donation form submission
            if ( !empty( $_POST["action"] ) && $_POST["action"] == 'user_donate' ) {
                $validation = $this->sx_validate_fields($_POST);

                if ( $validation["error"] ) {
                    $_SESSION["form_values"] = [
                        "first_name" => $_POST["first_name"],
                        "last_name" => $_POST["last_name"],
                        "ssn" => $_POST["ssn"],
                        "sum" => $_POST["sum"],
                        "charity" => $_POST["charity"],
                        "pay_gateway" => $_POST["payment_gateway"],
                        "accept" => $_POST["accept"],
                        "messages" => $validation["messages"],
                    ];

                    $url = (!empty($_SERVER["HTTP_REFERER"])) ? $_SERVER["HTTP_REFERER"] : $_SERVER["HTTP_ORIGIN"];

                    wp_redirect($url, 302);
                    die();
                } else {
                    $request_body = [
                        "transaction" => [
                            "amount"        => $_POST["sum"],
                            "currency"      => "eur",
                            "reference"     => "",
                            "merchant_data" => "",
                        ],
                        "customer" => [
                            "email"   => "",
                            "ip"      => $this->get_user_real_ip(),
                            "country" => "",
                            "locale"  => "",
                        ],
                    ];

                    $mc_response = $this->sxmc->maksekeskus->createTransaction($request_body);

                    if ( !empty( $mc_response->id ) ) {
                        $data = [
                            "first_name" => $_POST["first_name"],
                            "last_name" => $_POST["last_name"],
                            "ssn" => $_POST["ssn"],
                            "sum" => $_POST["sum"],
                            "charity" => $_POST["charity"],
                            "status" => $this->default_status,
                            "testing" => $this->sxmc->is_test,
                            "transaction" => $mc_response->id,
                        ];

                        $created = $this->sx_donations_create_post($data);

                        if ( $created ) {
                            unset($_SESSION["form_values"]);
                            wp_redirect($_POST["payment_gateway"] . $mc_response->id, 302);
                            die();
                        }

                    }

                }
            } else if ( !empty( $_REQUEST['json'] ) ) {
                $mc_response = json_decode( str_replace( '\\', '', $_REQUEST['json'] ) );

                if ( !empty( $mc_response->transaction ) ) {

                    $donation_id = $this->get_donation_by_transaction( $mc_response->transaction );

                    if ( !empty( $mc_response->status ) ) {
                        $status = $this->convert_mc_statuses( $mc_response->status );
                    }

                    if ( $donation_id && $status ) {

                        if ( $status == 'successful' ) {
                            $_SESSION['donation_type']    = 'ok';
                            $_SESSION['donation_message'] = 'successful';
                        } else if ( $status == 'cancelled' ) {
                            $_SESSION['donation_type']    = 'error';
                            $_SESSION['donation_message'] = 'cancelled';
                        } else {
                            $_SESSION['donation_type']    = 'error';
                            $_SESSION['donation_message'] = 'failed';
                        }

                        update_post_meta( $donation_id, 'status', $status );
                    }
                }

            } else {
                wp_redirect( home_url(), 302 );
                die();
            }
        }
    }

    /**
     * Get id of donation by transaction id
     *
     * @param $t_id
     * @return bool|int
     */
    public function get_donation_by_transaction( $t_id ){
        $response = 0;
        $args = array(
            'post_type'  => $this->post_type,
            'meta_query' => array(
                array(
                    'key'     => 'transaction',
                    'value'   => $t_id,
                )
            )
        );
        $query = new WP_Query( $args );

        if ( empty( $query ) || is_wp_error( $query ) ) {
            return false;
        }

        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post();
                $post = get_post();
                $response = $post->ID;
            }
        }

        return $response;
    }

    /**
     * Set up donation status based on received transaction status from MakeCommerce
     *
     * @param $mc_status
     * @return string
     */
    public function convert_mc_statuses( $mc_status ){
        switch ( $mc_status ){
            case 'CREATED':
            case 'PENDING':
                $status = 'pending';
                break;
            case 'CANCELLED':
                $status = 'cancelled';
                break;
            case 'EXPIRED':
            case 'DECLINED':
                $status = 'failed';
                break;
            case 'PART_REFUNDED':
            case 'REFUNDED':
                $status = 'refunded';
                break;
            case 'COMPLETED':
            case 'PAID':
                $status = 'successful';
                break;
            default:
                $status = '';
        }

        return $status;
    }

    /**
     * Output message about donation result
     *
     * @return string
     */
    public function donation_result(){
        $title = ( !empty( $_SESSION['donation_type'] ) ) ? $this->settings->get_setting_value( '_donation_message_' . $_SESSION['donation_type'] ) : '';
        $message = ( !empty( $_SESSION['donation_message'] ) ) ? $this->settings->get_setting_value( '_donation_message_' . $_SESSION['donation_message'] ) : '';

        return '<h2>' . $title . '</h2><p>' . $message . '</p>';
    }

}
