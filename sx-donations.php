<?php
/*
Plugin Name: SX Donations
Description: SX Donations is a plugin that allows website to accept donations made through bank API.
Version: 1.0.0
Author: Volodymyr Babak
License: this plugin is for code demonstration only. Usage by anyone but the author is prohibited.
*/

$params = [
    "textdomain" => "viazoi",
    "shortcode"  => "sx_donation_form",
    "endpoint"   => "sxdonation",
    "post_types" => ["charity" => "sx_charity", "donation" => "sx_donation"],
];

// load classes
spl_autoload_register( function ( $class_name ) {
    $classes_dir = plugin_dir_path( __FILE__ ) . '/classes/class-';
    $file = $classes_dir . strtolower( str_replace( '_', '-', $class_name ) ) . '.php';
    if( file_exists( $file ) ) require_once( $file );
} );

$sx_donations = new SX_Donations( $params );
